;;; go-up.el --- Go up in the hierarchy and possibly kill buffers by the way.  -*- lexical-binding: t ; eval: (view-mode 1) -*-


;; THIS FILE HAS BEEN GENERATED.


;;; Header:

;; Copyright 2017-2019 Marco Wahl

;; Author: Marco Wahl <marcowahlsoft@gmail.com>
;; Maintainer: Marco Wahl
;; Created: 2017-07-08
;; Keywords: convenience, navigation
;; URL: https://gitlab.com/marcowahl/go-up
;; Version: 0.0.4
;; Package-Requires: ((emacs "24"))

;; This file is not part of Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.


;;; Commentary:

;; See literate source file.  FIXME: generate docu for here from the
;; literate source.

;; Program
;; :PROPERTIES:
;; :ID:       b9b92b06-33fe-4091-aa65-15ffe6f73f89
;; :END:


;; [[file:go-up.org::*Program][Program:1]]

;;; Code:


;; Functions from other packages

(declare-function dired-goto-file "dired.el")
(declare-function dired-current-directory "dired.el")
(declare-function outline-level "outline.el")
(declare-function gnus-server-exit "gnus-srvr.el")
(declare-function eshell/pwd "em-dirs.el")
(declare-function eww-current-url "eww.el")
(declare-function magit-git-dir "magit-git.el")
(declare-function magit-section-up "magit-section.el")
(declare-function magit-section-backward "magit-section.el")
(declare-function magit-status-mode "magit-status.el")
(declare-function org-at-heading-p "org.el")
(declare-function org-before-first-heading-p "org.el" )
(declare-function org-up-element "org.el")
(declare-function org-element-at-point "org-element.el")
(declare-function org-element-type "org-element.el")
(declare-function org-up-heading-safe "org.el")
(declare-function speedbar-up-directory "speedbar.el")
(declare-function gnus-article-show-summary "gnus-art.el")
(declare-function gnus-summary-exit "gnus-sum.el")
(declare-function gnus-group-enter-server-mode "gnus-group.el")
(declare-function Info-up "info.el")


;;
(require 'compile) ;  compilation-directory


;;;###autoload
(defun go-up--find-alternate-dired (path)
  "Find dired containing PATH, position cursor there and kill previous buffer."
  (find-alternate-file
   (file-name-directory (directory-file-name path)))
  (dired-goto-file path))


;;;###autoload
(defun go-up ()
  "Go up dependent on context.
This is a collection of actions to go up in certain contexts.

This function often trys to open the corresponding directory in
`dired-mode' as an alternative buffer.

- Quit window.
- Replace dired with parent dired or kill buffer if root directory.
- Replace with dired.
- Go up one org-element in Org or replace with dired when at top of the
- buffer.
- Go up the hierachy in a magit buffer or open dired when at `point-min'.
- Replace eshell with dired.
- Replace compilation with dired.
- Replace eww with dired when visiting a local file.
- Replace Gnus article with Gnus summary.
- Replace Gnus summary with Gnus group.
- Replace Gnus group with Gnus server buffer.
- Replace Gnus server buffer with Gnus group buffer.
- In speedbar go to parent directory.
- Replace a file with its dired when no other rule applies."

  (interactive)
  (cond
   ((eq major-mode 'electric-buffer-menu-mode)
    (quit-window))
   ((eq major-mode 'dired-mode)
   (let ((dir (dired-current-directory)))
     (if (or (string-match (rx bol (1+ "/") eol) dir)
             (string-match (rx "/sudo:" (*? anything) ":/" eol) dir))
         (kill-buffer)
       (go-up--find-alternate-dired dir))
     nil))
   ((eq major-mode 'vc-dir-mode)
   (find-alternate-file (directory-file-name  default-directory)))
   ((memq major-mode '(magit-status-mode magit-diff-mode))
    (if (= 1 (point))
        (go-up--find-alternate-dired (magit-git-dir))
      (if (and
           (eq 'hunk (aref (get-text-property (point) 'magit-section) 1))
           (not (looking-at-p (rx bol "@@"))))
          (magit-section-backward)
        (magit-section-up)))
    nil)
   ((eq major-mode 'eshell-mode)
    (go-up--find-alternate-dired (concat (eshell/pwd) "/artificial-path-extension"))
    nil)
   ((eq major-mode 'compilation-mode)
    (go-up--find-alternate-dired
         (concat compilation-directory "/artificial-path-extension"))
    nil)
   ((eq major-mode 'eww-mode)
    (if (string-match-p
           "file://" (eww-current-url))
        (progn (go-up--find-alternate-dired
                (substring (eww-current-url) (length "file://")))
               nil)
      (if (bobp)
          (kill-buffer)
        (goto-char (point-min)))))
   ((eq major-mode 'org-mode)
    (if (= 1 (point))
        (if buffer-file-name
            (go-up--find-alternate-dired buffer-file-name)
          (kill-buffer))
          (if (= (point-min) (point))
              (widen)
            (let ((element-at-point (org-element-at-point)))
              (cond
               ((eq 'src-block (org-element-type element-at-point))
                (if (not (eq (point) (plist-get (cadr element-at-point) :begin)))
                    (goto-char (plist-get (cadr element-at-point) :begin))
                  (org-up-element)))
               (t (if (or (org-before-first-heading-p)
                          (and (org-at-heading-p)
                               (not (org-up-heading-safe))))
                      (goto-char (point-min))
                    (org-up-element) ; AFAICS there should be at least a
    				 ; subtree to go up to.
    		)))))))
   ((eq major-mode 'speedbar-mode)
    (if (string= "/" default-directory)
        (speedbar -1)
      (speedbar-up-directory)))
   ((eq major-mode 'gnus-article-mode)
    (gnus-article-show-summary)
    (delete-other-windows))
   ((eq major-mode 'gnus-summary-mode)
    (gnus-summary-exit))
   ((eq major-mode 'gnus-group-mode)
    (gnus-group-enter-server-mode))
   ((eq major-mode 'gnus-server-mode)
    (gnus-server-exit))
   ((eq major-mode 'Info-mode)
    (condition-case data
        (Info-up)
      (user-error
       (when (string= "Node has no Up" (cadr data))
         (find-alternate-file default-directory)))))
   ((or (< 1 (point-min)) (< (point-max) (1+ (buffer-size))))
    (widen))
   ((< (point-min) (point))
    (goto-char (point-min)))
   ((buffer-file-name)
    (go-up--find-alternate-dired buffer-file-name))
   (t
    (list-buffers)
    (message
     (concat
      "Buffer list.  No operation defined for this context."
      "  Is there a better alternative for go up for this context?"
      "  Consider to propose the idea or even respective code.")))))
;; Program:1 ends here


(provide 'go-up)


;;; go-up.el ends here
