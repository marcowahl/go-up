;;; test-features.el --- test go-up                                    -*- lexical-binding: t; -*-

(push (file-name-directory "~/p/elisp/mw/go-up/") load-path)
(require 'go-up)

(describe
 "go-up from magit status buffer."
 (it "go up from magit to dir."))

(describe
 "go-up from ordinary file."
 (it "go up from file to dir."))

(describe
 "go-up from dired"
 (it "go up from subdir to dir."
     (let* ((dirname1 "dir")
            (dirname2 "subdir"))
       (make-directory (concat dirname1 "/" dirname2) t)
       (cl-assert (file-exists-p (concat dirname1 "/" dirname2)))
       (dired (concat dirname1 "/" dirname2))
       (go-up)
       (expect
        (file-name-nondirectory
         (directory-file-name (dired-current-directory)))
        :to-equal dirname1)
       (kill-buffer)
       (delete-directory (concat "~/p/elisp/mw/go-up/" dirname1) t)))
 (it "not exists recent buffer.")
 (it "special: go-up in root dir."))

;;; test-features.el ends here
