LITERATE_SOURCE=go-up.org

ELISP_FILES=go-up.el

all: tangle doc

tangle: $(ELISP_FILES)

$(ELISP_FILES): $(LITERATE_SOURCE)
	emacs --batch --eval '(progn (find-file "$(LITERATE_SOURCE)") (org-babel-tangle))'

doc: $(LITERATE_SOURCE)
	emacs --batch $(LITERATE_SOURCE) --eval "(progn (require 'org) (require 'ox-md) ( org-export-to-file 'md \"README.md\" ))"
